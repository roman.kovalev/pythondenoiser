import tensorflow as tf
from tensorflow import keras
import numpy as np
from processor import *
from tensorflow.python.keras import Input
from tensorflow.python.keras.callbacks import EarlyStopping
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D, BatchNormalization, UpSampling2D
from tensorflow.python.keras.models import Model

from keras.layers import Dense, Flatten, Reshape, Input, InputLayer, MaxPool2D
from keras.models import Sequential, Model

mnist = tf.keras.datasets.mnist



needLearn = True

data = get_data_pure("/Users/romankovalev/src/Python/", "PetImages")
train_data = data[:3000]
test_data = data[3000:4000]
# gaussian_train_data = add_gaussian_to_dataset(train_data)
# gaussian_test_data = add_gaussian_to_dataset(test_data)


train_data = (train_data / 255.0).astype('float32') - 0.5
test_data = (test_data / 255.0).astype('float32') - 0.5
# gaussian_train_data = (gaussian_train_data / 255.0).astype('float32') - 0.5
# gaussian_test_data = (gaussian_test_data / 255.0).astype('float32') - 0.5


# def create_model():
#     x = Input(shape=(300, 300, 3))
#     # Encoder
#     e_conv1 = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
#     pool1 = MaxPooling2D((2, 2), padding='same')(e_conv1)
#     batchnorm_1 = BatchNormalization()(pool1)
#     e_conv2 = Conv2D(32, (3, 3), activation='relu', padding='same')(batchnorm_1)
#     pool2 = MaxPooling2D((2, 2), padding='same')(e_conv2)
#     batchnorm_2 = BatchNormalization()(pool2)
#     e_conv3 = Conv2D(16, (3, 3), activation='relu', padding='same')(batchnorm_2)
#     h = MaxPooling2D((2, 2), padding='same')(e_conv3)
#     # Decoder
#     d_conv1 = Conv2D(64, (3, 3), activation='relu', padding='same')(h)
#     up1 = UpSampling2D((2, 2))(d_conv1)
#     d_conv2 = Conv2D(32, (3, 3), activation='relu', padding='same')(up1)
#     up2 = UpSampling2D((2, 2))(d_conv2)
#     d_conv3 = Conv2D(16, (3, 3), activation='relu')(up2)
#     up3 = UpSampling2D((2, 2))(d_conv3)
#     r = Conv2D(3, (3, 3), activation='sigmoid', padding='same')(up3)
#     model = Model(x, r)
#     model.compile(optimizer='adam', loss='mse')
#     return model
#
# def learn():
#     gaussian_auto_encoder = create_model()
#     gaussian_early_stop = EarlyStopping(monitor='loss', patience=3)
#     gaussian_history = gaussian_auto_encoder.fit(gaussian_train_data, train_data, epochs=50, batch_size=32,
#                                                  callbacks=[gaussian_early_stop])
#
#     plt.plot(gaussian_history.epoch, gaussian_history.history['loss'])
#     plt.title('Epochs on Training Loss')
#     plt.xlabel('# of Epochs')
#     plt.ylabel('Mean Squared Error')
#     plt.show()
#
#     gaussian_auto_encoder.save('gaussian_noise.model')
#
#
# def load_model():
#     return keras.models.load_model('gaussian_noise.model')
#
# if needLearn:
#     learn()
# gaussian_auto_encoder = load_model()
#
#
# def predict_test():
#     result = gaussian_auto_encoder.predict(gaussian_test_data)
#     print(f'evaluate: {gaussian_auto_encoder.evaluate(test_data, gaussian_test_data)}')
#     return result
#
#
# def predict(data):
#     res = gaussian_auto_encoder.predict(prepare_check_data(data))
#     return res


def build_autoencoder(img_shape, code_size):
    # The encoder
    encoder = Sequential()
    encoder.add(InputLayer(img_shape))
    encoder.add(Flatten())
    encoder.add(Dense(code_size))

    # The decoder
    decoder = Sequential()
    decoder.add(InputLayer((code_size,)))
    decoder.add(Dense(np.prod(img_shape))) # np.prod(img_shape) is the same as 32*32*3, it's more generic than saying 3072
    decoder.add(Reshape(img_shape))

    return encoder, decoder

def build_autoencoder2():
    Input_img = Input(shape=(300, 300, 3))

    #encoding architecture
    x1 = Conv2D(256, (3, 3), activation='relu', padding='same')(Input_img)
    x2 = Conv2D(128, (3, 3), activation='relu', padding='same')(x1)
    x2 = MaxPool2D((2, 2))(x2)
    encoded = Conv2D(64, (3, 3), activation='relu', padding='same')(x2)

    # decoding architecture
    x3 = Conv2D(64, (3, 3), activation='relu', padding='same')(encoded)
    x3 = UpSampling2D((2, 2))(x3)
    x2 = Conv2D(128, (3, 3), activation='relu', padding='same')(x3)
    x1 = Conv2D(256, (3, 3), activation='relu', padding='same')(x2)
    decoded = Conv2D(3, (3, 3), padding='same')(x1)

    autoencoder = Model(Input_img, decoded)
    autoencoder.compile(optimizer='adam', loss='mse')
    return autoencoder


code_size = 1024

X_train = train_data
X_train_noise = add_gaussian_noise2(X_train)
X_test = test_data
X_test_noise = add_gaussian_noise2(X_test)

IMG_SHAPE = X_train.shape[1:]
# encoder, decoder = build_autoencoder(IMG_SHAPE, code_size)
#
# inp = Input(IMG_SHAPE)
# code = encoder(inp)
# reconstruction = decoder(code)
#
# autoencoder = Model(inp, reconstruction)
autoencoder = build_autoencoder2()
autoencoder.compile(optimizer='adamax', loss='mse')

print(autoencoder.summary())

# history = autoencoder.fit(x=X_train_noise, y=X_train, epochs=25, validation_data=(X_test_noise, X_test))

# for i in range(25):
#     print("Epoch %i/25, Generating corrupted samples..."%(i+1))
#     X_train_noise = add_gaussian_noise2(X_train)
#     X_test_noise = add_gaussian_noise2(X_test)
#
#     # We continue to train our model with new noise-augmented data
#     autoencoder.fit(x=X_train_noise, y=X_train, epochs=1,
#                     validation_data=(X_test_noise, X_test))

# for i in range(5):
#     img = X_test_noise[i]
#     visualize(img, encoder, decoder)


early_stopper = EarlyStopping(monitor='val_loss', min_delta=0.0001, patience=4, verbose=1, mode='auto')

a_e = autoencoder.fit(X_train_noise, X_train,
            epochs=50,
            batch_size=32,
            shuffle=True,
            validation_data=(X_test_noise, X_test),
            callbacks=[early_stopper])