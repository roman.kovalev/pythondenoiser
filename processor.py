import os
import numpy as np
import cv2
import pickle
import matplotlib.pyplot as plt

# this is a helper function to get the labels

def get_dirlist(rootdir):
    dirlist = []
    with os.scandir(rootdir) as folders:
        for entry in folders:
                path = str(entry.path)
                dirlist.append(path.replace(rootdir+'\\', ''))
    return dirlist


# retrieves the data from images for machine learning
def get_check_data(path):
    data = []
    for img in os.listdir(path):
        try:
            if img.endswith('jpg'):
                img_arr = cv2.imread(os.path.join(path, img), cv2.IMREAD_COLOR)
                resized_arr = cv2.resize(img_arr, (300, 300))
                print(os.path.join(path, img))
                data.append(np.asarray(resized_arr))
        except:
            print(f'ERROR processing %{os.path.join(path, img)}')
    return np.asarray(data)

def prepare_check_data(data):
    data = data / 255.0
    return np.asarray(data).astype('float32')


def get_data_pure(main_path, root_dir):
    data = []
    labels = get_dirlist(root_dir)
    for label in labels:
        path = os.path.join(main_path, label)
        for img in os.listdir(path)[:4000]:
            try:
                if img.endswith('jpg'):
                    img_arr = cv2.imread(os.path.join(path, img), cv2.IMREAD_COLOR)
                    resized_arr = cv2.resize(img_arr, (300, 300))
                    print(os.path.join(path, img))
                    data.append(np.asarray(resized_arr))
            except:
                print(f'ERROR processing %{os.path.join(path, img)}')
    return np.asarray(data).astype('uint8')

def get_data(main_path, x, y, root_dir):
    data = []
    labels = get_dirlist(root_dir)
    for label in labels:
        path = os.path.join(main_path, label)
        class_num = labels.index(label)
        for img in os.listdir(path)[:3000]:
            try:
                if img.endswith('jpg'):
                    img_arr = cv2.imread(os.path.join(path, img), cv2.IMREAD_COLOR)
                    print(os.path.join(path, img))
                    resized_arr = cv2.resize(img_arr, (x, y))
                    data.append([resized_arr, class_num])
            except:
                print(f'ERROR processing %{os.path.join(path, img)}')
    return data


# processes the data  c = 1 is gray scale and c = 3 is color

def process_data(data, x, y, c, feature_name, label_name):
    features = []
    labels = []
    for a, b in data:
        features.append(a)
        labels.append(b)
    features = np.array(features)
    labels = np.array(labels)

    # reshape features and labels for training
    features = features.reshape(-1, x, y, c)
    labels = labels.reshape(-1, x, y, c)
    pickle.dump(features, open(feature_name, 'wb'))
    pickle.dump(labels, open(label_name, 'wb'))


# adds the Gaussian noise based on the mean and the standard deviation
def add_gaussian_noise(data):
    mean = (1, 1, 1)
    std = (5, 5, 5)
    row, col, channel = data.shape
    noise = np.random.normal(mean, std, (row, col, channel)).astype('uint8')
    data_with_noise = data + noise
    np.clip(data_with_noise, 0, 255)
    return data_with_noise

def add_gaussian_noise2(X, sigma=0.1):
    noise = np.random.normal(loc=0.0, scale=sigma, size=X.shape).astype('float32')
    return (X + noise).astype('float32')

def add_gaussian_to_dataset(data):
    count = 0
    end = len(data)
    output_data = []
    while count < end:
        output_data.append(add_gaussian_noise2(data[count]))
        count += 1
    return np.asarray(output_data)


# matplotlib displays color in BGR format, we are converting that to RGB
def plot_rgb_img(img):
  plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
  plt.axis('off')
  plt.show()

def show_image(x):
    plt.imshow(np.clip(x + 0.5, 0, 1))

def visualize(img, encoder, decoder):
    """Draws original, encoded and decoded images"""
    # img[None] will have shape of (1, 32, 32, 3) which is the same as the model input
    code = encoder.predict(img[None])[0]
    reco = decoder.predict(code[None])[0]

    plt.subplot(1,3,1)
    plt.title("Original")
    show_image(img)

    plt.subplot(1,3,2)
    plt.title("Code")
    plt.imshow(code.reshape([code.shape[-1]//32,-1]))

    plt.subplot(1,3,3)
    plt.title("Reconstructed")
    show_image(reco)
    plt.show()